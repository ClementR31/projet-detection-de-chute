from application import app
from flask import render_template
import pandas as pd
import json
import plotly
import plotly.express as px



@app.route('/')
def ZeGraf():
    
    #DataFrame Adveez
    df=pd.read_parquet("/home/clement/Documents/DP/DF_Validation_v4")

    # Graph One
    #df = px.data.medals_wide()
    print('df')
    fig1 = px.line(df, x=df.index, y="FallDay", title="Prédiction de Chute")
    graph1JSON = json.dumps(fig1, cls=plotly.utils.PlotlyJSONEncoder)

    # Graph two
    df = px.data.iris()
    fig2 = px.scatter_3d(df, x='sepal_length', y='sepal_width', z='petal_width',
              color='species',  title="Iris Dataset")
    graph2JSON = json.dumps(fig2, cls=plotly.utils.PlotlyJSONEncoder)

    # Graph three
    df = px.data.gapminder().query("continent=='Oceania'")
    fig3 = px.line(df, x="year", y="lifeExp", color='country',  title="Life Expectancy")
    graph3JSON = json.dumps(fig3, cls=plotly.utils.PlotlyJSONEncoder)


    return render_template('index.html', graph1JSON=graph1JSON,  graph2JSON=graph2JSON, graph3JSON=graph3JSON)